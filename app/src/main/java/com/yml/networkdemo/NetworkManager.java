package com.yml.networkdemo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by SreeKumar on 24/7/15
 */
public class NetworkManager {

    /**
     * TAG Name
     **/
    private static final String TAG = NetworkManager.class.getSimpleName();

    /**
     * Callback to communicate results
     */
    public interface Callback {

        void onImageDownload(Bitmap bitmap);

        void onFailed();

    }

    /**
     * Download the image from the URL
     *
     * @param url
     * @param callback
     */

    public static void downloadImage(final String url, final Callback callback) {


        new AsyncTask<String, Integer, Bitmap>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);

            }

            @Override
            protected Bitmap doInBackground(String... params) {


                Bitmap bitmap = null;
                InputStream iStream = null;
                try {
                    URL url = new URL(params[0]);
                    /** Creating an http connection to communcate with url */
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                    /**Setting the type of the request **/
                    urlConnection.setRequestMethod("GET");


                    /** Connecting to url */
                    urlConnection.connect();

                    /** Reading data from url */
                    iStream = urlConnection.getInputStream();

                    /** Creating a bitmap from the stream returned from the url */
                    bitmap = BitmapFactory.decodeStream(iStream);

                } catch (Exception e) {
                    Log.d(TAG, "Exception while downloading url" + e.toString());
                } finally {
                    try {
                        iStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return bitmap;


            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);

                if (bitmap != null)
                    callback.onImageDownload(bitmap);
                else
                    callback.onFailed();


            }
        }.execute(url);


    }


}
