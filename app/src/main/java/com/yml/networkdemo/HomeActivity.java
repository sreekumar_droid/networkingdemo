package com.yml.networkdemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class HomeActivity extends AppCompatActivity implements NetworkManager.Callback {

    private static final String URL = "https://s-media-cache-ak0.pinimg.com/736x/0b/c3/54/0bc3540bfe0bed032cfdbe4e3d0b7a61.jpg";
/** Hold the Image reference **/
    private ImageView mImage;
    /**Show Progress**/
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final Button download = (Button) findViewById(R.id.download);
        mImage = (ImageView) findViewById(R.id.image);
        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Downloading...");
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {
                    NetworkManager.downloadImage(URL, HomeActivity.this);
                    mProgress.show();
                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Checking the Network status
     * @return
     */
    private boolean isNetworkAvailable() {
        boolean available = false;
        /** Getting the system's connectivity service */
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        /** Getting active network interface  to get the network's status */
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isAvailable())
            available = true;

        /** Returning the status of the network */
        return available;
    }


    @Override
    public void onImageDownload(Bitmap bitmap) {

        mImage.setImageBitmap(bitmap);
        mProgress.dismiss();

    }

    @Override
    public void onFailed() {

        mProgress.dismiss();

    }
}
